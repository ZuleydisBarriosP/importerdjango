from rest_framework import serializers
from importerApp.models import *


class ReadDataFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReadDataFile
        fields = ("id", "region", "order_date", "order_id", "total")
