from django.db import models


class UploadFile(models.Model):
    file_path = models.FileField(upload_to="uploaded")
    created_date = models.DateTimeField(auto_now_add=True)


class ReadDataFile(models.Model):
    region = models.CharField(max_length=45)
    order_date = models.DateField()
    order_id = models.IntegerField()
    total = models.FloatField()
