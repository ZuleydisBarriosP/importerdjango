from django.urls import path
from importerApp.views import ReadDataFileViewSet, uploadfile

from rest_framework import routers

router = routers.DefaultRouter()
router.register(r"read-file", ReadDataFileViewSet)


urlpatterns = [path("", uploadfile, name="uploadfile")]
