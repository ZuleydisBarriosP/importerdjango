from django.apps import AppConfig


class ImporterappConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "importerApp"
