from django.contrib import admin

from importerApp.models import ReadDataFile, UploadFile

# Register your models here.
@admin.register(UploadFile)
class UploadFileAdmin(admin.ModelAdmin):
    list_display = ["id", "file_path", "created_date"]


@admin.register(ReadDataFile)
class ReadDataFile(admin.ModelAdmin):
    list_display = ["id", "region", "order_date", "order_id", "total"]
