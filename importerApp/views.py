import datetime

import numpy as np
import pandas as pd
import pathlib
from django.shortcuts import render

from importerApp.models import ReadDataFile, UploadFile

from rest_framework import viewsets
from importerApp.serializers import ReadDataFileSerializer


class ReadDataFileViewSet(viewsets.ModelViewSet):
    queryset = ReadDataFile.objects.all()
    serializer_class = ReadDataFileSerializer

    # permission_classes = (permissions.IsAuthenticated,)


def uploadfile(request):
    if request.method == "POST":
        file_uploaded = request.FILES["file_path"]
        # extension = pathlib.Path(file_uploaded.name).suffix
        upfile = UploadFile.objects.create(file_path=file_uploaded)
        process_data(upfile.file_path.name)
    return render(
        request,
        "uploadfile.html",
        context={
            "readDataFile": ReadDataFile.objects.all(),
        },
    )


def process_data(uploadedfile):
    data = extract_file(uploadedfile)
    data = clean_data(data)
    save_data(data)


def extract_file(filename):
    csv_data = pd.read_csv("media/" + filename)
    return csv_data


def clean_data(csv_data):
    for item in range(csv_data["Order Date"].size):
        try:
            csv_data.at[item, "Order Date"] = pd.to_datetime(
                csv_data["Order Date"].iloc[item]
            ).date()
        except ValueError:
            csv_data.at[item, "Order Date"] = np.NaN
    return csv_data.dropna().reset_index(drop=True)


def save_data(data):
    for row in data.itertuples(index=False):
        ReadDataFile.objects.create(
            region=row[0],
            order_date=row[1],
            order_id=row[2],
            total=row[3],
        )
